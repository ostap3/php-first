<?php

declare(strict_types=1);

namespace App\Task1;

use Webmozart\Assert\Assert;

class Track
{
    private array $cars;
    private float $lapLength;
    private int $lapsNumber;

    public function __construct(float $lapLength, int $lapsNumber)
    {
        Assert::greaterThan($lapLength, 0, 'A track $lapLength must be a positive integer. Got: %s');
        Assert::greaterThan($lapsNumber, 0, 'A track $lapsNumber must be a positive integer. Got: %s');


        $this->lapLength = $lapLength;
        $this->lapsNumber = $lapsNumber;
    }

    public function getLapLength(): float
    {
        return $this->lapLength;
    }

    public function getLapsNumber(): int
    {
        return $this->lapsNumber;
    }

    public function add(Car $car): void
    {
        $this->cars[] = $car;
    }

    public function all(): array
    {
        if (empty($this->cars)) {
            throw new \ErrorException('You must add cars before use');
        }

        return $this->cars;
    }

    public function run(): Car
    {
        return collect($this->all())
            ->sortBy('speed')
            ->sortBy('pitStopTime')
            ->sortBy('fuelConsumption')
            ->sortBy('fuelTankVolume')
            ->first();
    }
}