<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\Car;
use App\Task1\Track;

class CarTrackHtmlPresenter
{
    public function present(Track $track): string
    {
        $response = '';

        /** @var Car $car */
        foreach ($track->all() as $car) {
            $response .= <<<HTML
<figure>
    <img src="{$car->getImage()}">
    <figcaption>{$car->getName()}: {$car->getSpeed()}, {$car->getPitStopTime()}</figcaption>
</figure>
HTML;
        }

        return $response;
    }
}