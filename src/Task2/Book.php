<?php

declare(strict_types=1);

namespace App\Task2;

use Webmozart\Assert\Assert;

class Book
{
    protected string $title;
    protected int $price;
    protected int $pagesNumber;

    public function __construct(string $title, int $price, int $pagesNumber)
    {
        Assert::greaterThan($price, 0, 'A book $price must be a positive integer. Got: %s');
        Assert::greaterThan($pagesNumber, 0, 'A book $pagesNumber must be a positive integer. Got: %s');

        $this->title = $title;
        $this->price = $price;
        $this->pagesNumber = $pagesNumber;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getPagesNumber(): int
    {
        return $this->pagesNumber;
    }
}